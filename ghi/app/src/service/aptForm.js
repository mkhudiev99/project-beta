import React from 'react';

class ServiceAppointmentForm extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            VIN: '',
            customer_name: '',
            date: '',
            technicians: [],
            technician: '',
            reason: '',
            // finished: false,

        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleVIN = this.handleVIN.bind(this);
        this.handleCustomerName = this.handleCustomerName.bind(this);
        this.handleDate = this.handleDate.bind(this);
        this.handleTechnician = this.handleTechnician.bind(this);
        this.handleReason = this.handleReason.bind(this)

    }
    async componentDidMount() {
        const techUrl = 'http://localhost:8080/api/technician/';

        const response = await fetch(techUrl);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({ technicians: data.technician });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }
        data.customer_name = data.customerName
        delete data.customer_name;

        const aptUrl = 'http://localhost:8080/api/services/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(aptUrl, fetchConfig);
        if (response.ok) {
            const newApt = await response.json();
            console.log(newApt);
            this.setState({
                VIN: '',
                customer_name: '',
                date: '',
                technicians: [],
                technician: '',
                reason: '',
            });
        }
    }
    handleVIN(event) {
        const value = event.target.value
        this.setState({ VIN: value })
    }
    handleCustomerName(event) {
        const value = event.target.value
        this.setState({ customer_name: value })
    }
    handleDate(event) {
        const value = event.target.value
        this.setState({ date: value })
    }
    handleTechnician(event) {
        const value = event.target.value
        this.setState({ technician: value })
    }
    handleReason(event) {
        const value = event.target.value
        this.setState({ reason: value })
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Schedule an Appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-service-appointment-form">

                            <div className="form-floating mb-3">
                                <input value={this.state.VIN} onChange={this.handleVIN} placeholder="VIN" required type="text" name="VIN" id="VIN" className="form-control" />
                                <label htmlFor="VIN">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.customer_name} onChange={this.handleCustomerName} placeholder="customer_name" name="customer_name" type="text" id="customer_name" className="form-control" />
                                <label htmlFor="customer_name">Customer Name</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input value={this.state.date} onChange={this.handleDate} placeholder="date" name="date" type="date" id="date" className="form-control" />
                                <label htmlFor="date">Date</label>
                            </div>



                            <div className="mb-3">
                                <div className="form-floating mb-3">
                                    <input value={this.state.reason} onChange={this.handleReason} placeholder="reason" name="color" type="text" id="reason" className="form-control" />
                                    <label htmlFor="reason">Reason</label>
                                </div>
                                <select value={this.state.technician} onChange={this.handleTechnician} required id="technician" name="technician" className="form-select">
                                    <option value="">Pick your technician</option>
                                    {this.state.technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.id}>
                                                {technician.technician_name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }

}
export default ServiceAppointmentForm;