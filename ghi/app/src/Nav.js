import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>

          <div className="dropdown">
            <button className="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
            </button>
            <div className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
              <li><NavLink className="dropdown-item" aria-current="page" to="manufacturers/new/">New Manufacturer</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="manufacturers/">All Manufacturers</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="models/new/">New Vehicle Model</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="models/">All Models</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="automobiles/new/">New Automobile</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="automobiles/">All Automobiles</NavLink></li>
            </div>
          </div>
          <div className="dropdown">
            <button className="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
      
            </button>
            <div className="dropdown-menu dropdown-menu-dark open" aria-labelledby="dropdownMenuButton2">
              <li><NavLink className="dropdown-item" aria-current="page" to="technicians/new/">Create a Technician</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="technicians/">All Technician</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="services/new">Create Service</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="/services/history/">Service History</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="services/">Service Appointments</NavLink></li>
            </div>
          </div>
        </button>
        </div>
      </nav>
  )
}



export default Nav;
