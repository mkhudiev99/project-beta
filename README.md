# CarCar

Team:

* Person 1 - Which microservice?
* Murad Khudiev 2 - Service

## Design

## Service microservice

The CarCar serves a solution to your vehicle matters. The CarCar can handle multipled services like creating an appointment to service your vehicle and storing all the data about the vehicle, its owner and the party provoding a service. The Service microservice in particular will allow you to generate vehicles, technicians, and service appointments and save them on your page.


## Start the Service micro

I initially clone the repository and open it with VSCode while building and running the servers on the Docker Desktop and setting up the Insmonia page for prospective views.

My React app is running on port 3000 while the admin page is on port 8080. 

I start off with registering the app in the settings file for the project.

We then move onto creating the Models for the service.

I created 3 models for the service called: AutomobileVO, Technician and ServiceAppointment.

## Models

AutomobileVO has import_href and VIN as its parameters which will be the platform for creating and storing new automobiles.

Technician has a name and employee_number

ServiceAppointment has a VIN, customer_name, date, reason, shares whether it is finished or not as well as has a foreignkey relationship to a technician that is assigned to a certain appointment.


## Admin page

We need to go in the admin file, import and register the models we just created like this:

``` @admin.register(ServiceAppointment)
class ServiceAppointment(admin.ModelAdmin):
    pass
```

## Views

We will need to create views for all our models in order to perform certain functionailities. We first import our models to the views.py file and the create our view functions.


api_list_technician
api_delete_technician

For Technician, our view must allow us to get all instances of the technician, therefore all their names and information. We should be able to create or delete a technician and assign a certain employee_number to them.


api_list_appointment
api_show_appointments
api_service_history

When we get to the ServiceAppointment views it gets a little more tricky because we have a foreignkey relationship with technician and every service appointment is tied to a certain technician specialist. Views for ServiceAppointment model allow us to create, adjust and delete the apointment instances for a vehicle. We also create a view for the ServiceHistory which takes the VIN number of the vehicle and checks if the vehicle is registered with the dealership already in which case offering that client a VIP treatment.

api_list_automobileVO

The view for AutombolileVO allows us to create and store vehicles for the later use to create the appointments.


## URLs

After creating the views, we need to establish the paths to reach that service on the web page so we need to set up the urls importing and using the view functions that we created and using a proper path so the site can be found and reached.






## Sales microservice

Explain your models and integration with the inventory
microservice, here.
