import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest.models import AutomobileVO

def get_vin():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    print(content)
    for auto in content["autos"]:
        AutomobileVO.objects.update_or_create(VIN=auto["VIN"])


def poll():
    while True:
        print('automobile was purchased from dealership, customer gets VIP discount.')
        try:
            get_vin()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(15)

if __name__ == "__main__":
    poll()