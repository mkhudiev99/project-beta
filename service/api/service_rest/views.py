from django.shortcuts import render
from .models import Technician, ServiceAppointment, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
# Create your views here.



class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]


# @require_http_methods(["GET", "POST"])
# def api_list_technician(request):
#     if request.method == "GET":
#         technician = Technician.objects.all()
#         return JsonResponse({"technicians": technician}, encoder=TechnicianEncoder)
#     else:
#         content = json.loads(request.body)
#         try:
#             technician = content["technician"]
#             technician = Technician.objects.create(**content)
            
#             return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
#         except:
#             response = JsonResponse({"message: Could not create technician"})
#             response.status_code = 400
#             return response

@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technician": technicians}, encoder = TechnicianEncoder, safe=False)
    else: 
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)



@require_http_methods(["DELETE"])
def api_delete_technician(request, pk):
    if request.method == "DELETE":
        technician = Technician.objects.get(id=pk)
        technician.delete()
        return JsonResponse({"technician": technician}, encoder = TechnicianEncoder, safe = False)


class AutomobileVOEncoder(ModelEncoder):
   model = AutomobileVO
   properties = [
    "VIN",
    "import_href"
    ]


class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "VIN",
        "customer_name",
        "date",
        "reason",
        "id",
    ]

    encoders = {
         "technician": TechnicianEncoder(),
         "vin": AutomobileVOEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "technician": o.technician.name,
        }




@require_http_methods(["GET", "POST"])
def api_list_appointment(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse({
            "appointments": appointments
            }, encoder=ServiceAppointmentEncoder, safe=False)
    else: 
        content = json.loads(request.body)
        try:
            name = content["technician"]
            technician = Technician.objects.get(name=name)
            content["technician"] = technician

            vin=content["VIN"]
            vin_number=AutomobileVO.objects.get(VIN=vin)
            content["VIN"]=vin_number
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid auto"}, status=400)
        ServiceAppointment.objects.get()
        appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(appointment, encoder=ServiceAppointmentEncoder, safe=False)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointments(request, VIN):
    if request.method == "GET":
        try:
            appointment = ServiceAppointment.objects.get(id=VIN)
            return JsonResponse(appointment, encoder=ServiceAppointmentEncoder, safe=False)
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "appointment does not exist"}, status=400)
    elif request.method == "DELETE":
        try:
            appointment = ServiceAppointment.objects.get(id=VIN)
            appointment.delete()
            return JsonResponse(appointment, encoder = ServiceAppointmentEncoder, safe = False)
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "appointment does not exist"}, status = 400)







@require_http_methods(["PUT"])
def api_cancel(request, pk):
    app = ServiceAppointment.objects.get(id=pk)
    app.cancel()
    return JsonResponse(app, encoder=ServiceAppointmentEncoder, safe=False)

@require_http_methods(["PUT"])
def api_finish(request, pk):
    app = ServiceAppointment.objects.get(id=pk)
    app.finish()
    return JsonResponse(app, encoder=ServiceAppointmentEncoder, safe=False)




@require_http_methods(["GET"])
def api_list_automobileVO(request):
    if request.method == "GET":
        automobile = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobile": automobile},
            encoder = AutomobileVOEncoder, safe=False)


@require_http_methods(["GET"])
def api_service_history(request, VIN):
    appointments = ServiceAppointment.objects.filter(id=VIN)
    return JsonResponse(
        appointments,
        encoder=ServiceAppointmentEncoder, safe=False)




