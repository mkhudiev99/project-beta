from django.contrib import admin
from .models import Technician, ServiceAppointment, AutomobileVO
# Register your models here.




@admin.register(ServiceAppointment)
class ServiceAppointment(admin.ModelAdmin):
    pass

@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass

# @admin.register(Status)
# class Status(admin.ModelAdmin):
#     pass

@admin.register(Technician)
class Technician(admin.ModelAdmin):
    pass


