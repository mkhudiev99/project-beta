from time import time
from django.db import models
from django.urls import reverse

# Create your models here.

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, null=True)
    VIN = models.CharField(max_length=200, unique=True)


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name", "employee_number", "id")

# class Status(models.Model):
#     id = models.PositiveSmallIntegerField(primary_key=True)
#     name = models.CharField(max_length=200, null=True)

#     def __str__(self):
#         return self.name

#     class Meta:
#         ordering = ("id", "name")


class ServiceAppointment(models.Model):
    VIN = models.CharField(max_length=200, unique=True)
    customer_name = models.CharField(max_length=200)
    date = models.DateTimeField(null=True)
    reason = models.CharField(max_length=200)
    technician = models.ForeignKey(Technician, related_name="appointments", on_delete = models.CASCADE)
    finished = models.BooleanField(default=False)

    # status = models.ForeignKey(Status, related_name="appointments", on_delete=models.PROTECT)

    # def finish(self):
    #     status = Status.objects.get(name="FINISHED")
    #     self.status = status
    #     self.save()

    # def cancel(self):
    #     status = Status.objects.get(name="CANCELED")
    #     self.status = status
    #     self.save()



